# Deploy a local kubernetes cluster using Vagrant and VirtualBox.
Deploys a kubernetes cluster using virtualbox headless resources.

## Usage:
```
cd local_kube
vagrant up
```

## Requirements
- Vagrant 2.2.4
- VirtualBox
- Ansible - if running on linux (SEE NOTES)

## TODO:
Need to remove the commented lines ansible adds to /etc/docker/daemon.json
Currently, the ansible provisioner fails because it doesn't recognize these as comments. To complete the build,
these lines are deleted manually and
```
vagrant provision [k8-master...node-X]
```
is run.

## NOTES:
- The Calico Network version is presently hardcoded. Past versions of the network have proved buggy. Check for an update if the build fails
during the "apply [projectcalico]".
- Vagrant is set to use the ansible "ansible_local" provisioner because I'm deploying the cluster on a windows machine - which Ansible doesn't support
as a deploy server. You may use the basic ansible provisioner to speed up the build if you are running vagrant on a linux box that has ansible installed.
SEE (1). 

## REF:

1. [Vagrant and Ansible Setup from Kubernetes.io Blog](https://kubernetes.io/blog/2019/03/15/kubernetes-setup-using-ansible-and-vagrant/)
2. [Calico Kubernetes Network](https://docs.projectcalico.org/v3.9/getting-started/kubernetes/)
